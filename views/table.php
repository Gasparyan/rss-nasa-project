<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Bootstrap 101 Template</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	</head>
	<body>
		<h1>Nasa RSS</h1>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<div class="table-responsive"> 
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Actions</th>
						<th><a href="view.php">Title</a></th>
						<th><a href="https://www.nasa.gov/">Publication date</a></th>
						<th>Upload date</th>
						<th>Comments</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>3</td>
						<td>Edit or remove</td>
						<td>The ice fields</td>
						<td>23.08.2019 08:00</td>
						<td>23.07.2019 08:00</td>
						<td>5</td>
					</tr>
				</tbody>
				</table>
		</div>
	</body>
</html>